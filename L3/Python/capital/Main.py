from cours.bloc1.capital.villes.Ville import Ville
from cours.bloc1.capital.villes.Capitale import Capitale

ville1 = Ville("New-York", 8623000)
ville2 = Ville("Lyon")

print(ville1)
print(ville2)

capital1 = Capitale("Paris", "France", 2141000)
print(capital1)

print("catégorie de la ville de " , ville1.getNom() , " : " , ville1.categorie())
print("catégorie de la ville de " , capital1.getNom() , " : " , capital1.categorie())