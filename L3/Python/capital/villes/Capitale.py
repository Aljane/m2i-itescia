from cours.bloc1.capital.villes.Ville import Ville


class Capitale(Ville):

    def __init__(self, nom, pays, nbhabitants=0):
        super().__init__(nom, nbhabitants)
        self.pays = pays.upper()

    def getPays(self):
        return self.pays

    def setPays(self, pays):
        self.pays = pays

    def __str__(self):
        return "La ville est : " + self.nom + " elle est peuplé de : " + str(self.nbHabitants) + " du pays " + self.pays

    def categorie(self):
        return 'C'
