class Voiture:
    def __init__(self):
        self.modele = 'Mondeo'
        self.immat = '000XX00'
        self.dateCirculation = '10-09-2000'

    def getmodele(self):
        return self.modele

    def getimmat(self):
        return self.immat

    def getdateCirculation(self):
        return self.dateCirculation

    def setmodele(self,modele):
        self.modele = modele

    def setimmat(self,immat):
        self.immat = immat

    def setdateCirculation(self,dateCirculation):
        self.dateCirculation = dateCirculation

    def estFr(self):
        if self.immat[:3].isdigit() and not self.immat[3:5].isdigit() and self.immat[5:7].isdigit() and int(self.immat[5:7]) >= 1 and int(self.immat[5:7]) <= 97:
            return True
        return False

    def estDeCollection(self):
        now = datetime.datetime.now()
        if (now.year - 20) >= int(self.dateCirculation[6:11]):
            return True
        return False

    def __str__(self):
        return "Modele: " + self.modele + " l'immat est: " + self.immat + " date: " + self.dateCirculation


V1 = Voiture()
print (V1)

    
