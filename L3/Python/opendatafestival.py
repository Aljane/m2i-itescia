import string
from tkinter import Label, StringVar, Tk, RAISED, Entry
from tkinter import ttk, Button, Label, RAISED, StringVar, Tk
from reportlab.pdfgen import canvas
from reportlab.lib.units import cm
from fpdf import FPDF, HTMLMixin
from tkinter import ttk, Button, Label, RAISED, StringVar, Tk
import pymysql.cursors

root = Tk()
root.geometry("1150x400")
root.title('IHM')

label = Label(root, text="Python Open Data Festival")
label.pack()

tree = ttk.Treeview(root)

mydb = pymysql.connect(
        host="mysql-aljane.alwaysdata.net",
        user="aljane",
        passwd="OZYKUPJHPQPXGSLK",
        database="aljane_bdd"
    )

tree["columns"] = ("one", "two", "three","four", "five","six", "seven",)
tree.column("one", width=250)
tree.column("two", width=150)
tree.column("three", width=280)
tree.column("four", width=120)
tree.column("five", width=85)
tree.column("six", width=80)
tree.column("seven", width=75)

tree.heading("#0", text='Numéro', anchor='w')
tree.column("#0", anchor="w", width=55)
tree.heading("one", text="Nom de la manifestation")
tree.heading("two", text="Domaines")
tree.heading("three", text="Site web")
tree.heading("four", text="Commune principale")
tree.heading("five", text="Date de debut")
tree.heading("six", text="Date de fin")
tree.heading("seven", text="Code postal")

mycursor = mydb.cursor()
sql_select_Query = "SELECT * FROM PythonOpenDataFestival"
mycursor.execute(sql_select_Query)
records = mycursor.fetchall()

cpt = 1  # Counter representing the ID of your code.
for row in records:
    tree.insert('', 'end', text=str(cpt), values=(row[1], row[2], row[3],row[4], row[5],row[6], row[7]))
    cpt += 1  # increment the ID

tree.pack()

label = Label(root, text="Saissisez votre recherche : ")
label.pack()

# entrée
value = StringVar()
value.set("texte par défaut")
entree = Entry(root, textvariable=string, width=30)
entree.pack()

def callback():
    tree.delete(*tree.get_children())
    r1 = entree.get()
    mycursor = mydb.cursor()
    #sql_select_Query = "SELECT * FROM `PythonOpenDataFestival` WHERE `Date_de_debut`= " + "'" + r1 + "'" "or `Date_de_fin` = " + "'" + r1 + "'" " or `Commune_principale` = " + "'" + r1 + "'" "or `Domaines` = " + "'" + r1 + "'"
    sql_select_Query = "SELECT * FROM `PythonOpenDataFestival` WHERE `Date_de_debut` LIKE " + "'" + r1 + "%'" "or `Date_de_fin` LIKE " + "'" + r1 + "%'" " or `Commune_principale` LIKE " + "'" + r1 + "%'" "or `Domaines` LIKE " + "'" + r1 + "%'"

    mycursor.execute(sql_select_Query)
    records = mycursor.fetchall()

    cpt = 1  # Counter representing the ID of your code.
    for row in records:
        tree.insert('', 'end', text=str(cpt), values=(row[1], row[2], row[3], row[4], row[5], row[6], row[7]))
        cpt += 1  # increment the ID

    nbResultat = cpt - 1

    print('Recherche effectuer: ' + entree.get() + ' (' + str(nbResultat) + ' résultats) ')


Button(text='Valider', command=callback).pack()

def callback2():
    class HTML2PDF(FPDF, HTMLMixin):
        pass

    def html2pdf():
        html = '''<h1 align="center">PyFPDF HTML Demo</h1>
        <p>This is regular text</p>
        <p>''' + row[1] + '''</b>, <i>italicize</i> or <u>underline</u></br>
        '''

        #html = '''<h1 align="center">PyFPDF HTML Demo</h1>
        #<p>This is regular text</p>
        #<p>You can also <b>bold</b>, <i>italicize</i> or <u>underline</u>
        #'''

        #html = '''<tr>
        #<td>Bonjour, je suis votre première cellule </td>
        #<td>je suis votre deuxième cellule </td>
        #<td>je suis votre troisième cellule </td>
        #<td>je suis votre quatrième cellule </td>
        #</tr>'''
        pdf = HTML2PDF()
        pdf.add_page()
        pdf.write_html(html)



        #maData = row[1] + " " + row[2] + " " + row[3] + " " + row[4] + " " + row[5] + " " + row[6] + " " + row[7]
        maData = row[1] + " " + row[2] + " " + row[3] + " " + str(row[4]) + " " + str(row[5]) + " " + str(row[6]) + " " + str(row[7])
        pdf.write_html(maData)

        pdf.output('html2pdf.pdf')


    if __name__ == '__main__':
        html2pdf()

Button(text='Générer PDF', command=callback2).pack()

bouton = Button(root, text="Fermer", command=root.quit)
bouton.pack()


root.mainloop()
