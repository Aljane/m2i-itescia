from cours.bloc1.starwars.films.Film import Film
from cours.bloc1.starwars.films.acteurs.personnages.Personnage import Personnage
from cours.bloc1.starwars.films.acteurs.personnages.types.Gentil import Gentil
from cours.bloc1.starwars.films.acteurs.personnages.types.Mechant import Mechant
from cours.bloc1.starwars.films.acteurs.Acteur import Acteur
from cours.bloc1.starwars.films.FilmsManager import FilmsManager

"""
2)
film1 = Film("La Menace Fantôme", 1999, 1, 100000000, 100000000)
titre=input(print("Quelle est le titre ?))
annee=input(print("Quelle annee est il sorti ?))
numeroEpisode=input(print("C'est le combientieme ?))
cout=input(print("Combien a t'il couter ?))
recette=input(print("Combien a t'il apporter ?))
film2 = Film(titre,annee,numeroEpisode,cout,recette)
print(film2)
3)
personnage1 = Personnage("Obi-Wan", "Kenobi")
4)
film = Film("La Menace Fantôme", 1999, 1, 100000000, 100000000)
acteur1 = Acteur("Obi-Wan", "Kenobi")
acteur2 = Acteur("Skywalker", "Anakin")
film.addActeur(acteur1)
film.addActeur(acteur2)
6)
film.fetchActeur()
8)
film.getActeur(0).addPersonnage(Personnage("Obi-Wan", "Kenobi"))
film.getActeur(0).addPersonnage(Personnage("Skywalker", "Anakin"))
9)
print(film.getActeur(0).getNbPersonnages())


film = Film("Star Wars, épisode IV", 1977, 1, 145000000, 780000000)
film.addActeurs([Acteur("Aljane", "Mohamed-Ali"), Acteur("Skywalker", "Anakin")])
#exercice tableau
film.getActeur(0).addPersonnage(Gentil("Bom1", "Prénom1", True))
film.getActeur(0).addPersonnage(Mechant("Xom2", "Prénom2", False))

film.tri()
print(film.fetchActeur())"""


filmsManager= FilmsManager()
film = Film("Star Wars, épisode V", 1998, 2, 1668900, 3569990)
film2 = Film("Star Wars, épisode III", 1978, 3, 1893678, 3569990)
filmsManager.addFilm(film)
filmsManager.addFilm(film2)
filmsManager.makeBackUp()

