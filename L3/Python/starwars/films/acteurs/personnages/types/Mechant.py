from cours.bloc1.starwars.films.acteurs.personnages.Personnage import Personnage


class Mechant(Personnage):
    def __init__(self, nom, prenom, coteObscur):
        super().__init__(nom, prenom)
        self.coteObscur=coteObscur

    def getNom(self):
        return self.nom

    def getPrenom(self):
        return self.prenom

    def getcoteObscur(self):
        return self.coteObscur

    def setNom(self, nom):
        self.nom = nom

    def setPrenom(self, prenom):
        self.prenom = prenom

    def setForce(self, coteObscur):
        self.force = coteObscur

    def __str__(self):
        return "Nom: " + self.nom + " Prenom: " + self.prenom + " Cote obscur: " + self.coteObscur